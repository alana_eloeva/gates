//
//  TVHeader.swift
//  GateProject
//
//  Created by Alana on 26.10.17.
//  Copyright © 2017 Alana. All rights reserved.
//

import UIKit

class TVHeader: UITableViewCell {

    @IBAction func switchD_action(_ sender: Any) {
        if (sender as AnyObject).isOn {
            print("Диагностика проведена - ", (sender as AnyObject).tag)
        }
        else{
            print("Отменить")
        }
    }
    @IBAction func switchR_action(_ sender: Any) {
        if (sender as AnyObject).isOn{
            print("Требуется ремонт - ", (sender as AnyObject).tag)
        }else{
            print("Отменить")
        }
    }
    
    @IBOutlet weak var labelDiagn: UILabel!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var switchDiagn: UISwitch!
    @IBOutlet weak var switchRepair: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
