//
//  TVCell.swift
//  GateProject
//
//  Created by Alana on 29.09.17.
//  Copyright © 2017 Alana. All rights reserved.
//

import UIKit

class TVCell:UITableViewCell {

    static let ID = "ExpandableCell"
    @IBOutlet weak var switchD: UISwitch!
    @IBOutlet weak var switchR: UISwitch!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var labelDiagn: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func switchTopButton_Action(_ sender: UISwitch) {
        if sender.isOn {
          print("Диагностика проведена - ", sender.tag)
        }
        else{
             print("Отменить")
        }
    }
    @IBAction func switchRButton_Action(_ sender: UISwitch) {
        if sender.isOn{
            print("Требуется ремонт - ", sender.tag)
        }else{
            print("Отменить")
        }
    }
}
