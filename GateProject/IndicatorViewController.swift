//
//  IndicatorViewController.swift
//  GateProject
//
//  Created by user on 30.03.2018.
//  Copyright © 2018 Alana. All rights reserved.
//

import UIKit

class IndicatorViewController: UIViewController {
    
    @IBOutlet var myView: UIView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        indicator.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        let transform: CGAffineTransform = CGAffineTransform(scaleX: 2.5, y: 2.5)
        indicator.transform = transform
        indicator.center = self.view.center
        indicator.startAnimating()
        self.myView.addSubview(indicator)
    }
}
