//
//  ClientDataVC.swift
//  GateProject
//
//  Created by Alana on 18.09.17.
//  Copyright © 2017 Alana. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import Toast_Swift

class ClientDataVC: UIViewController{
    
    @IBOutlet weak var clientName: UITextField!
    @IBOutlet weak var clientEmail: UITextField!
    @IBOutlet weak var clientCityStreet: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var organizationName: UITextField!
    
    var arrModels = [String]()
    var models = [ModelGroups]()
    var workName: String = ""
    
    var orderTypeID = -1
    let appdelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        
        let clearImage = UIImage(named: "close")!
        self.clientName.clearButtonWithImage(clearImage)
        self.clientEmail.clearButtonWithImage(clearImage)
//        self.clientCityStreet.clearButtonWithImage(clearImage)
        self.organizationName.clearButtonWithImage(clearImage)
        
        clientCityStreet.text = appdelegate.location
        
        (UIApplication.shared.delegate as! AppDelegate).getBaseDataAsync { (base: Base) in
            self.arrModels.removeAll()
            for mg in base.modelGroups! {
                self.arrModels.append(mg.eq_name!)
                self.models.append(mg)
            }
            if (self.arrModels.count>0){
                self.tableView.reloadData()
            }
        }
        
        let name = UserDefaults.standard.string(forKey: "clientName")
        let org = UserDefaults.standard.string(forKey: "organizationName")
        let email = UserDefaults.standard.string(forKey: "email")
    
    
        if(name != nil){
            clientName.text = UserDefaults.standard.string(forKey: "clientName")
        }
        
        if(org != nil){
            organizationName.text = UserDefaults.standard.string(forKey: "organizationName")
            
        }
        
        if(email != nil){
            clientEmail.text = UserDefaults.standard.string(forKey: "email")
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (appdelegate.isEmptyField(textField: organizationName, message: "Поле 'Название организации' не заполнено")){
            if (appdelegate.isEmptyField(textField: clientCityStreet, message: "Поле 'Город, улица' не заполнено")){
                if (appdelegate.isEmptyField(textField: clientName, message: "Поле 'ФИО' не заполнено")) {
                    if (appdelegate.isEmptyField(textField: clientEmail, message: "Поле 'e-mail' не заполнено")){
                        if (appdelegate.validateEmail(enteredEmail: clientEmail.text!, message: "Поле 'e-mail' заполнено некорректно")){
                            let appdelegate = UIApplication.shared.delegate as! AppDelegate
                            let a = models[indexPath.row].models
                            if(a?.count == 0 ){
                                let vc = storyboard?.instantiateViewController(withIdentifier: "WorkListVC") as! WorkListController
                                appdelegate.modelGroupIndex = 0
                                self.navigationController?.pushViewController(vc, animated: true)
                            }else{
                                let vc2 = storyboard?.instantiateViewController(withIdentifier: "ModelsVC") as! ModelsVC
                                appdelegate.modelGroupIndex = indexPath.row
                                self.navigationController?.pushViewController(vc2, animated: true)
                            }
                            appdelegate.clientName = clientName.text!
                            appdelegate.gateType = Int(models[indexPath.row].eq_id!)!
                            appdelegate.email = clientEmail.text!
                            appdelegate.organizationName = organizationName.text!
                            
                            let defaults = UserDefaults.standard
                            defaults.set(clientName.text!, forKey: "clientName")
                            defaults.set(organizationName.text!, forKey: "organizationName")
                            defaults.set(clientEmail.text!, forKey: "email")
                            defaults.synchronize()
                        }
                    }
                }
            }
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    func isNumber(textField: UITextField, message: String) -> Bool {
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: textField.text!)
        if allowedCharacters.isSuperset(of: characterSet) {
            return true
        } else {
            self.view.makeToast(message, duration: 3.0, position: CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.width / 5))
            
            
            return false
        }
    }
}

extension ClientDataVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:UITableViewCell
        cell = (tableView.dequeueReusableCell(withIdentifier: "cell"))!
        let model = arrModels[indexPath.row]
        cell.textLabel?.text = model
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

extension UITextField{
    func clearButtonWithImage(_ image: UIImage) {
        let clearButton = UIButton()
        clearButton.setImage(image, for: .normal)
        clearButton.frame = CGRect(x: 0, y: 0, width: 30, height: 20)
        clearButton.contentMode = .scaleAspectFit
        clearButton.addTarget(self, action: #selector(self.clear(sender:)), for: .touchUpInside)
        self.rightView = clearButton
        self.rightViewMode = .always
    }
    
    func clear(sender: AnyObject) {
        self.text = ""
    }
}
