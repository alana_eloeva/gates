//
//  OrderTypeVC.swift
//  GateProject
//
//  Created by Alana on 22.12.17.
//  Copyright © 2017 Alana. All rights reserved.
//

import UIKit

class OrderTypeVC: UIViewController{
    
    @IBOutlet weak var tableview: UITableView!
    
    var arrOrderID = [String]()
    var arrOrders = [Orders]()
    var identies = [String]()
    
    let appdelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        (UIApplication.shared.delegate as! AppDelegate).getBaseDataAsync { (base:Base!) in
            self.arrOrderID.removeAll()
            for orders in base.orders!{
                self.arrOrders.append(orders)
                self.arrOrderID.append(orders.act_id!)
            }
            if (self.arrOrderID.count>0){
                self.tableview.reloadData()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension OrderTypeVC: UITableViewDelegate, UITableViewDataSource{
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderTypeCell") as! OrderTVCell
        cell.dateLabel.text = arrOrders[indexPath.row].act_date
        cell.adressLabel.text = arrOrders[indexPath.row].clnt_addr
        cell.orderNumber.text = arrOrders[indexPath.row].act_id!
        cell.clientLabel.text = arrOrders[indexPath.row].clnt_name
        return cell
    }
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOrderID.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "WorkListVC") as! WorkListController
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        if(arrOrders[indexPath.row].mdl_id == nil){
            appdelegate.modelGroupIndex = 0
            
        }else{
            appdelegate.modelGroupIndex = Int(arrOrders[indexPath.row].mdl_id!)!
        }
//        appdelegate.cycles = arrOrders[indexPath.row].act_cycles!
        appdelegate.workIndex = Int(arrOrders[indexPath.row].act_work_type!)!
        appdelegate.clientName = arrOrders[indexPath.row].clnt_resp!
        appdelegate.gateType = Int(arrOrders[indexPath.row].act_mg!)!
        appdelegate.email = "-"
        appdelegate.order_act_id = arrOrders[indexPath.row].act_id!
        appdelegate.organizationName = arrOrders[indexPath.row].clnt_name!
        
        self.navigationController?.pushViewController(vc, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

