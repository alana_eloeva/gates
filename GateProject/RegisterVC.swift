//
//  RegisterVC.swift
//  GateProject
//
//  Created by Alana on 20.10.17.
//  Copyright © 2017 Alana. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class RegisterVC: UIViewController {
    let url = "http://msofter.ru/gates/reg.php"
    @IBOutlet weak var regBtn: UIButton!
    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    let preferences = UserDefaults.standard
    
    let appdelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginField.text = self.preferences.string(forKey: "login")
        passwordField.text = self.preferences.string(forKey: "pass")
    }
    
    @IBAction func register(_ sender: Any) {
        
        appdelegate.addIndicator()
        
        if(!(loginField.text?.isEmpty)! && !(passwordField.text?.isEmpty)!){
            let parameters = [
                "login": loginField.text!,
                "pass": passwordField.text!
            ]
            
            Alamofire.request(url, method: .post, parameters: parameters).responseString(completionHandler: { (response: DataResponse<String>) in
                switch response.result{
                case .success(let responseObject):
                    self.preferences.set(parameters["login"], forKey: "login")
                    self.preferences.set(parameters["pass"], forKey: "pass")
                    self.preferences.synchronize()
                    
                    var num  :Int? = nil
                    num = Int(responseObject)
                    if (responseObject.contains("ERR") || num == nil){
                        self.view.makeToast(responseObject)
                        self.appdelegate.hideIndicator()
                    } else {
                        
                        let appdelegate = UIApplication.shared.delegate as! AppDelegate
                        appdelegate.userId = num!
                        
                        print(responseObject)
                        (UIApplication.shared.delegate as! AppDelegate).UserID = responseObject
                        appdelegate.hideIndicator()
                        self.nextScreen()
                        appdelegate.hideIndicator()
                    }
                case .failure(let error):
                    print(error)
                    self.view.makeToast("Ошибка авторизации", duration: 3.0, position: CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.width / 5))
                    self.appdelegate.hideIndicator()
                }
            })
        } else{
            self.view.makeToast("Заполните поля!", duration: 3.0, position: CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.width / 5))
        }
    }
    
    func nextScreen() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let secondViewController = storyBoard.instantiateViewController(withIdentifier: "chooseOrderScreen")
        self.present(secondViewController, animated:true, completion:nil)
    }
}
