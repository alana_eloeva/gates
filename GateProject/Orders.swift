//
//  Orders.swift
//  GateProject
//
//  Created by Alana on 22.12.17.
//  Copyright © 2017 Alana. All rights reserved.
//

import Foundation
import ObjectMapper

public class Orders: Mappable{
    public var act_id : String?
    public var act_user : String?
    public var act_сlient : String?
    public var act_serial : String?
    public var act_cycles : String?
    public var act_mg : String?
    public var act_date : String?
    public var act_status : String?
    public var act_work_type : String?
    public var clnt_name : String?
    public var clnt_resp : String?
    public var clnt_addr : String?
    public var mdl_id : String?
    public var mdl_name : String?
    
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [Orders]
    {
        var models:[Orders] = []
        for item in array
        {
            models.append(Orders(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required public init?(dictionary: NSDictionary) {
        act_id = dictionary["act_id"] as? String
        act_user = dictionary["act_user"] as? String
        act_date = dictionary["act_date"] as? String
        act_сlient = dictionary["act_client"] as? String
        act_serial = dictionary["act_serial"] as? String
        act_cycles = dictionary["act_cycles"] as? String
        act_mg = dictionary["act_mg"] as? String
        act_status = dictionary["act_status"] as? String
        act_work_type = dictionary["act_work_type"] as? String
        clnt_name = dictionary["clnt_name"] as? String
        clnt_resp = dictionary["clnt_resp"] as? String
        clnt_addr = dictionary["clnt_addr"] as? String
        mdl_id = dictionary["mdl_id"] as? String
        mdl_name = dictionary["mdl_name"] as? String
    }
    
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        dictionary.setValue(self.act_id, forKey: "act_id")
        dictionary.setValue(self.act_user, forKey: "act_user")
        dictionary.setValue(self.act_сlient, forKey: "act_client")
        dictionary.setValue(self.act_serial, forKey: "act_serial")
        dictionary.setValue(self.act_cycles, forKey: "act_cycles")
        dictionary.setValue(self.act_mg, forKey: "act_mg")
        dictionary.setValue(self.act_status, forKey: "act_status")
        dictionary.setValue(self.act_work_type, forKey: "act_work_type")
        dictionary.setValue(self.clnt_name, forKey: "clnt_name")
        dictionary.setValue(self.clnt_resp, forKey: "clnt_resp")
        dictionary.setValue(self.clnt_addr, forKey: "clnt_addr")
        dictionary.setValue(self.mdl_id, forKey: "mdl_id")
        dictionary.setValue(self.mdl_name, forKey: "mdl_name")
        dictionary.setValue(self.act_date, forKey: "act_date")
        
        return dictionary
    }
    
    public required init?(map: Map) {
        act_id <- map["act_id"]
        act_date <- map["act_date"]
        act_user <- map["act_user"]
        act_сlient <- map["act_client"]
        act_serial <- map["act_serial"]
        act_cycles <- map["act_cycles"]
        act_mg <- map["act_mg"]
        act_status <- map["act_staatus"]
        act_work_type <- map["act_work_type"]
        clnt_name <- map["clnt_name"]
        clnt_resp <- map["clnt_resp"]
        clnt_addr <- map["clnt_addr"]
        mdl_id <- map["mdl_id"]
        mdl_name <- map["mdl_name"]
    }
    
    public func mapping(map: Map) {
        act_id <- map["act_id"]
        act_user <- map["act_user"]
        act_date <- map["act_date"]
        act_сlient <- map["act_client"]
        act_serial <- map["act_serial"]
        act_cycles <- map["act_cycles"]
        act_mg <- map["act_mg"]
        act_status <- map["act_staatus"]
        act_work_type <- map["act_work_type"]
        clnt_name <- map["clnt_name"]
        clnt_resp <- map["clnt_resp"]
        clnt_addr <- map["clnt_addr"]
        mdl_id <- map["mdl_id"]
        mdl_name <- map["mdl_name"]
        
    }
}


