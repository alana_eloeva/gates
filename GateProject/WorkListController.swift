//
//  WorkListController.swift
//  GateProject
//
//  Created by David Minasyan on 28.10.17.
//  Copyright © 2017 Alana. All rights reserved.
//

import Foundation
import ExpyTableView
import UIKit

class WorkListController: UIViewController {
    
    @IBOutlet weak var expandableTableView: ExpyTableView!
    
    @IBOutlet weak var infoLabel: UILabel!
    struct IPath {
        var row: Int
        var section: Int
    }
    
    var delegate = UIApplication.shared.delegate as! AppDelegate;
    var arrWG = [Wg]()
    var indexes = [IPath]()
    
    public var workName:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        self.infoLabel.isHidden = true
        
        arrWG = [Wg]()
        (UIApplication.shared.delegate as! AppDelegate).getBaseDataAsync(cmpl: { (b:Base) in
            for w in (b.modelGroups?[appdelegate.modelGroupIndex].wg)!{
                if(Int(w.wtgmg_acs!)! == appdelegate.workIndex){
//                    self.infoLabel.isHidden = true
                    self.arrWG.append(w)
                }else{
                    self.infoLabel.isHidden = false
                }
            }
            self.expandableTableView.dataSource = self
            self.expandableTableView.reloadData()
            
            if(self.arrWG.count != 0){
                 self.infoLabel.isHidden = true
            }else{
                 self.infoLabel.isHidden = false
            }
            
        })
        expandableTableView.allowsSelection = false
        expandableTableView.dataSource = self
    }
    
    @IBAction func diagnWorkSwitchChage(_ sender: Any) {
        let sw = sender as! UISwitch
        if sw.isOn{
            delegate.addToWorkDiagn(wt: (arrWG[indexes[sw.tag].section].workTypes?[indexes[sw.tag].row])!)
        } else {
            delegate.removeFromWorkDiagn(wt: (arrWG[indexes[sw.tag].section].workTypes?[indexes[sw.tag].row])!)
        }
    }
    
    @IBAction func repairWorkSwitchChage(_ sender: Any) {
        let sw = sender as! UISwitch
        if sw.isOn{
            delegate.addToWorkRepair(wt: (arrWG[indexes[sw.tag].section].workTypes?[indexes[sw.tag].row])!)
        } else {
            delegate.removeFromWorkRepair(wt: (arrWG[indexes[sw.tag].section].workTypes?[indexes[sw.tag].row])!)
        }
    }
    
    @IBAction func diagnGroupSwitchChage(_ sender: Any) {
        let sw = sender as! UISwitch
        if sw.isOn{
            delegate.addToGroupDiagn(wt: arrWG[sw.tag])
            expandableTableView.expand(sw.tag)
        } else {
            delegate.removeFromGroupDiagn(wt: arrWG[sw.tag])
            if (!delegate.isInGroupRepair(wg: arrWG[sw.tag])){
                expandableTableView.collapse(sw.tag)
            }
        }
    }
    
    @IBAction func repairGroupSwitchChage(_ sender: Any) {
        let sw = sender as! UISwitch
        if sw.isOn{
            delegate.addToGroupRepair(wt: arrWG[sw.tag])
            expandableTableView.expand(sw.tag)
        } else {
            delegate.removeFromGroupRepair(wt: arrWG[sw.tag])
            if (!delegate.isInGroupDiagn(wg: arrWG[sw.tag])){
                expandableTableView.collapse(sw.tag)
            }
        }
    }
}

extension WorkListController: ExpyTableViewDataSource{
    
    private func isInIndexes(indexPath: IndexPath) -> Bool{
        for ind in indexes{
            if (ind.section == indexPath.section && ind.row == indexPath.row){
                return true
            }
        }
        return false
    }
    
    private func indexOf(indexPath: IPath) -> Int{
        var i = 0;
        for ind in indexes{
            if (ind.section == indexPath.section && ind.row == indexPath.row){
                return i
            }
            i+=1;
        }
        let path = IPath(row: indexPath.row, section: indexPath.section)
        indexes.append(path)
        return i;
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrWG.count
    }
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let ip = IPath(row: indexPath.row-1, section: indexPath.section)
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TVCell
        cell.label.text = arrWG[ip.section].workTypes?[ip.row].wt_name;
        let i = indexOf(indexPath: ip)
        cell.switchD.tag = i
        cell.switchR.tag = i
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        
        if (appdelegate.workIndex < 1){
            cell.switchD.isHidden = true
            cell.labelDiagn.isHidden = true
        }
        
        if (delegate.isInWorkDiag(workType: (arrWG[ip.section].workTypes?[ip.row])!)){
            cell.switchD.isOn = true
        } else {
            cell.switchD.isOn = false
        }
        
        if (delegate.isInWorkRepair(workType: (arrWG[ip.section].workTypes?[ip.row])!)){
            cell.switchR.isOn = true
        } else {
            cell.switchR.isOn = false
        }
        
        return cell
    }
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (arrWG[section].workTypes?.count)!+1
    }
    
    func expandableCell(forSection section: Int, inTableView tableView: ExpyTableView) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "cellHeader") as! TVHeader
        cell.label.text = arrWG[section].wtg_name
        cell.switchDiagn.tag = section
        cell.switchRepair.tag = section
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        if (appdelegate.workIndex < 1){
            cell.switchDiagn.isHidden = true
            cell.labelDiagn.isHidden = true
        }
        
        if delegate.isInGroupDiagn(wg: arrWG[section]){
            cell.switchDiagn.isOn = true
        } else {
            cell.switchDiagn.isOn = false
        }
        
        if delegate.isInGroupRepair(wg: arrWG[section]){
            cell.switchRepair.isOn = true
        } else {
            cell.switchRepair.isOn = false
        }
        return cell
    }
    
    override func didMove(toParentViewController parent: UIViewController?) {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        if(parent == nil){
            appdelegate.workRepair.removeAll()
            appdelegate.workDiagn.removeAll()
            appdelegate.groupDiagn.removeAll()
            appdelegate.groupRepair.removeAll()
        }
    }
}
