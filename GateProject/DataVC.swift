//
//  DataVC.swift
//  GateProject
//
//  Created by Alana on 03.10.17.
//  Copyright © 2017 Alana. All rights reserved.
//

import UIKit
import Alamofire

class DataVC: UIViewController{
    
    @IBOutlet weak var sendBtn: UIButton!
//    @IBOutlet weak var textLabel: UILabel!
//    @IBOutlet weak var secondSend: UIButton!
    @IBOutlet weak var additionalInformTextView: UITextField!
//    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var streetLabel: UILabel!
    @IBOutlet weak var workTypeLabel: UILabel!
    @IBOutlet weak var gateTypeLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var gateModelLabel: UILabel!
    @IBOutlet weak var currentState: UITextField!
    @IBOutlet weak var emailLabelForOrders: UITextField!
    @IBOutlet weak var serNumber: UITextField!
    @IBOutlet weak var cycleCount: UITextField!
    @IBOutlet weak var organizationName: UILabel!
    @IBOutlet weak var serNumberLabel: UILabel!
    @IBOutlet weak var cycleCountLabel: UILabel!
    @IBOutlet weak var emailFOLabel: UILabel!
    
    public var act_id: String!
    public var cStreet: String!
    public var workType : Int!
    public var gateModel: Int!
    public var contents:String = ""
    public var addInform: String = " "
    public var fileCount = 0
    public var list = [String]()
    var gateType : String!
    var writePath : String!
    var cEmail : String!
    
    let appdelegate = UIApplication.shared.delegate as! AppDelegate
    var parameters: Dictionary<String, Any> = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        
//        if(appdelegate.gateType == 2){
//            
//            serNumber.attributedPlaceholder = NSAttributedString(string:"XX-123456 или XXX-123456", attributes: [NSForegroundColorAttributeName: UIColor.gray])
//
//        }
        
        nameLabel.text = appdelegate.clientName
        streetLabel.text = appdelegate.location
        workTypeLabel.text = String(appdelegate.workIndex)
        gateTypeLabel.text = String(appdelegate.gateType)
        emailLabel.text = appdelegate.email
        gateModelLabel.text = String(appdelegate.modelId)
        addInform = additionalInformTextView.text!
        organizationName.text = appdelegate.organizationName
        
        colorLabel(label: serNumberLabel)
        colorLabel(label: cycleCountLabel)
        colorLabel(label: emailFOLabel)
        
        let clearImage = UIImage(named: "close")!
        self.emailLabelForOrders.clearButtonWithImage(clearImage)
        
        let savedEmail = UserDefaults.standard.string(forKey: "email2")
        if(savedEmail != nil){
            emailLabelForOrders.text = UserDefaults.standard.string(forKey: "email2")
        }
    }
    
    func colorLabel(label: UILabel){
        var myMutableString = NSMutableAttributedString(string: label.text!, attributes: [NSFontAttributeName:UIFont.systemFont(ofSize: 19)])
        myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.red, range: NSRange(location:(label.text?.count)!-1,length:1))
        // set label Attribute
        label.attributedText = myMutableString
    }
    
    func getFileCount () -> Int {
        if (fileCount > 0){
            appdelegate.list = list
            return fileCount
        }
        else{
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let fm = FileManager.default
            do{
                list = try fm.contentsOfDirectory(atPath: documentsPath)
                appdelegate.list = list
                print (list)
                fileCount = list.count
                return list.count
            }
            catch{
            }
            return 0
        }
    }
//
//    @IBAction func secondSendToServer(_ sender: Any) {
//        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
//        for item in list{
//            do {
//                let fullName = documentsPath.appendingFormat("/%@", item)
//                let text = try String(contentsOfFile: fullName)
//                let dictContents = self.convertToDictionary(text: text as String)
//
//                Alamofire.request("http://msofter.ru/gates/receive.php", method: .post, parameters: dictContents, encoding: JSONEncoding.default)
//                    .responseString(completionHandler: { (response : DataResponse<String>) in
//                        switch response.result{
//
//                        case .failure(let error):
//                            print(error)
//                            self.view.makeToast("Не отправлено", duration: 3.0, position: CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.width / 5))
//                            break
//                        case .success(let responseObject):
//
//                            print(responseObject)
//                            self.view.makeToast("Отправлено", duration: 3.0, position: CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.width / 5))
//
//
//                            let fileManager = FileManager.default
//                            do {
//                                try fileManager.removeItem(atPath: fullName)
//                                self.fileCount-=1
////                                self.tableView.reloadData()
//                            }
//                            catch let error as NSError {
//                                print("Something went wrong: \(error)")
//                            }
//                        }
//                    })
//            } catch {
//                // contents could not be loaded
//            }
//        }
//    }
    
    @IBAction func saveDataPressed(_ sender: Any) {
        if(getData() != nil){
            self.saveData()
            appdelegate.list = list
            nextScreen()
            self.view.window?.makeToast("Сохранено", duration: 3.0, position: CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.width / 5))
        }
    }
    
    public func getData() -> Dictionary<String, Any>?{
        if(appdelegate.isEmptyField(textField: serNumber, message: "Поле 'Серийный номер' не заполнено")){
                if(appdelegate.isEmptyField(textField: cycleCount, message: "Поле 'Количество циклов' не заполнено")){
                    if(appdelegate.isEmptyField(textField: emailLabelForOrders, message: "Поле 'e-mail' не заполнено")){
                        if(appdelegate.validateEmail(enteredEmail: emailLabelForOrders.text!, message: "Поле 'e-mail' заполнено неккоректно")){
                            
                            let formatter = DateFormatter()
                            formatter.dateFormat = "yyyy-MM-dd-HH-mm"
                            let date = Date()
        
                            let email2 : String = emailLabelForOrders.text!
                            let defaults = UserDefaults.standard
                            defaults.set(email2, forKey: "email2")
                            
                            parameters = [
                                "clientName": appdelegate.clientName,
                                "clientStreet": appdelegate.location,
                                "selectedWorkType": appdelegate.workIndex,
                                "selectedGateType": appdelegate.gateType,
                                "userID": appdelegate.userId,
                                "orgName" :appdelegate.organizationName,
                                "cycles" :cycleCount.text!,
                                "serNumb" : serNumber.text!,
                                "date":formatter.string(from: date),
                                "email":appdelegate.email,
                                "act_id":appdelegate.order_act_id,
                                "email2": email2,
                                "model": appdelegate.modelId,
                                "additionalInform": addInform,
                                "currentState": currentState.text!,
                                "dataRepair":jsonToString(json: (UIApplication.shared.delegate as! AppDelegate).workRepair.toJSON() as AnyObject),
                                "dataDiag":jsonToString(json: (UIApplication.shared.delegate as! AppDelegate).workDiagn.toJSON() as AnyObject),
                                "groupRepair": jsonToString(json: (UIApplication.shared.delegate as! AppDelegate).groupRepair.toJSON() as AnyObject),
                                "groupDiagn": jsonToString(json: (UIApplication.shared.delegate as! AppDelegate).groupDiagn.toJSON() as AnyObject)
                                ] as [String : AnyObject]
                            
                            return parameters
                        }
                        
                    }
            }
            
        }
        return nil
        
    }
    
    @IBAction func sendToServer(_ sender: Any) {
        if getData() != nil{
            
            appdelegate.addIndicator()
            
            var userID = appdelegate.UserID
            if (userID == nil){
                userID = "-1"
            }
            
            Alamofire.request("http://msofter.ru/gates/receive.php", method: .post, parameters: getData(), encoding: URLEncoding.default)
                .responseString(completionHandler: { (response: DataResponse<String>) in
                    switch response.result {
                    case .failure(let error):
                        self.appdelegate.hideIndicator()
                        
                        print(error)
                        self.view.makeToast("Не отправлено", duration: 3.0, position: CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.width / 5))
                        if let data = response.data, let responseString = String(data: data, encoding: .utf8) {
                            print(responseString)
                           self.saveData()
                        }
                    case .success(let responseObject):
                        self.appdelegate.hideIndicator()
                        print(responseObject)
                        self.parent?.view.makeToast("Отправлено", duration: 3.0, position: CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.width / 5))
                        
                        self.nextScreen()
                    }
                    (sender as! UIButton).isEnabled = true
                })
        }
    }
    
    public func saveData(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd-HH-mm"
        let date = Date()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self.getData(), options: .prettyPrinted)
            let convertedString = String(data: jsonData, encoding: String.Encoding.utf8)
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            self.writePath = (NSURL(fileURLWithPath: documentsPath).appendingPathComponent(formatter.string(from: date))?.path)!
            try convertedString?.write(toFile: self.writePath, atomically: true, encoding: String.Encoding.utf8)
            self.fileCount+=1
            fileCount = list.count
            
//            self.tableView.reloadData()
        } catch {
            print(error.localizedDescription)
        }
        
    }
    
    func jsonToString(json: AnyObject) -> String{
        do {
            let data1 =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
            let convertedString = String(data: data1, encoding: String.Encoding.utf8) // the data will be converted to the string
            print(convertedString ?? "") // <-- here is ur string
            return convertedString!
            
        } catch let myJSONError {
            print(myJSONError)
            return ""
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: String]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func indicatorShow(){
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "indicatorVC") as! IndicatorViewController
        vc.view.frame = self.view.frame
        self.view.addSubview(vc.myView)
        vc.didMove(toParentViewController: self)
    }
    
    
    func nextScreen() {
        self.navigationController?.popToRootViewController(animated: false)
    }
}

//extension DataVC: UITableViewDataSource{
//    @available(iOS 2.0, *)
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
//        cell?.textLabel?.text = list[indexPath.row]
//        return cell!
//    }
//
//    @available(iOS 2.0, *)
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return getFileCount()
//    }
//}


