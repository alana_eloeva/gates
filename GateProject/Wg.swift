/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
import ObjectMapper
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class Wg:Mappable{
	public var wtg_id : String?
	public var wtg_name : String?
	public var wtg_extra : String?
	public var wtgmg_id : String?
	public var wtgmg_wtg : String?
	public var wtgmg_mg : String?
	public var wtgmg_acs : String?
    public var workTypes : [WorkTypes]?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let wg_list = Wg.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Wg Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Wg]
    {
        var models:[Wg] = []
        for item in array
        {
            models.append(Wg(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let wg = Wg(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Wg Instance.
*/
	required public init?(dictionary: NSDictionary) {

		wtg_id = dictionary["wtg_id"] as? String
		wtg_name = dictionary["wtg_name"] as? String
		wtg_extra = dictionary["wtg_extra"] as? String
		wtgmg_id = dictionary["wtgmg_id"] as? String
		wtgmg_wtg = dictionary["wtgmg_wtg"] as? String
		wtgmg_mg = dictionary["wtgmg_mg"] as? String
		wtgmg_acs = dictionary["wtgmg_acs"] as? String
        if (dictionary["workTypes"] != nil) { workTypes = WorkTypes.modelsFromDictionaryArray(array: dictionary["workTypes"] as! NSArray) }
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.wtg_id, forKey: "wtg_id")
		dictionary.setValue(self.wtg_name, forKey: "wtg_name")
		dictionary.setValue(self.wtg_extra, forKey: "wtg_extra")
		dictionary.setValue(self.wtgmg_id, forKey: "wtgmg_id")
		dictionary.setValue(self.wtgmg_wtg, forKey: "wtgmg_wtg")
		dictionary.setValue(self.wtgmg_mg, forKey: "wtgmg_mg")
		dictionary.setValue(self.wtgmg_acs, forKey: "wtgmg_acs")

		return dictionary
	}
    
    public required init?(map: Map) {
        wtg_id <- map["wtg_id"]
        wtg_name <- map["wtg_name"]
        wtg_extra <- map["wtg_extra"]
        wtgmg_id <- map["wtgmg_id"]
        wtgmg_wtg <- map["wtgmg_wtg"]
        wtgmg_mg <- map["wtgmg_mg"]
        wtgmg_acs <- map["wtgmg_acs"]
        workTypes <- map["workTypes"]
    }
    
    public func mapping(map: Map) {
        wtg_id <- map["wtg_id"]
        wtg_name <- map["wtg_name"]
        wtg_extra <- map["wtg_extra"]
        wtgmg_id <- map["wtgmg_id"]
        wtgmg_wtg <- map["wtgmg_wtg"]
        wtgmg_mg <- map["wtgmg_mg"]
        wtgmg_acs <- map["wtgmg_acs"]
        workTypes <- map["workTypes"]

    }

}
