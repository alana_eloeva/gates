//
//  ChooseOrderTypeViewController.swift
//  GateProject
//
//  Created by Alana on 21.12.17.
//  Copyright © 2017 Alana. All rights reserved.
//

import UIKit
import CoreLocation

class ChooseOrderTypeViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var tableview: UITableView!
    var ordertype = [String]()
    var identities = [String]()
    var arrModels = [String]()
    var clickedID = -1
    
    var locationManager = CLLocationManager()
    var location: CLLocation?
    let geocoder = CLGeocoder()
    var placemark: CLPlacemark?
    var city = ""
    var country = ""
    var countryShortName = ""
    
    let appdelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewWillAppear(_ animated: Bool) {
        determineMyCurrentLocation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ordertype = ["Сформировать заказ", "Заказ - наряд"]
        identities = ["createOrder", "chooseOrder"]
        
        (UIApplication.shared.delegate as! AppDelegate).getBaseDataAsync { (base: Base) in }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if(country.isEmpty || city.isEmpty){
            appdelegate.addIndicator()
            return false
        }
        return true
    }
}

extension ChooseOrderTypeViewController : UITableViewDataSource, UITableViewDelegate{
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ordercell")!
        let text = ordertype [indexPath.row]
        cell.textLabel?.text = text
        return cell
    }
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ordertype.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cName = identities[indexPath.row]
        if(cName == "chooseOrder"){
            let vc = storyboard?.instantiateViewController(withIdentifier: cName) as! OrderTypeVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if(cName == "createOrder"){
            let vc2 = storyboard?.instantiateViewController(withIdentifier: cName) as! MyTableViewController
            vc2.orderID = indexPath.row
            self.navigationController?.pushViewController(vc2, animated: true)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
        stopLocationManager()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let latestLocation = locations.last!
        if latestLocation.horizontalAccuracy < 0 {
            return
        }
        if location == nil || location!.horizontalAccuracy > latestLocation.horizontalAccuracy {
            location = latestLocation
            stopLocationManager()
            
            geocoder.reverseGeocodeLocation(latestLocation, completionHandler: { (placemarks, error) in
                if error == nil, let placemark = placemarks, !placemark.isEmpty {
                    self.placemark = placemark.last
                }
                self.parsePlacemarks()
            })
        }
    }
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    func stopLocationManager() {
        locationManager.stopUpdatingLocation()
        locationManager.delegate = nil
    }
    
    func parsePlacemarks() {
        appdelegate.showIndicator()
        if let _ = location {
            if let placemark = placemark {
                if let city = placemark.locality, !city.isEmpty {
                    self.city = city
                }
                if let country = placemark.thoroughfare, !country.isEmpty {
                    self.country = country
                }
                print(country, city)
                appdelegate.location = "\(city), \(country)"
                appdelegate.hideIndicator()
            }
            
        } else {
            
        }
    }
}
