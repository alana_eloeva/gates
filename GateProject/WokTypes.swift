//
//  WokTypes.swift
//  GateProject
//
//  Created by Alana on 16.09.17.
//  Copyright © 2017 Alana. All rights reserved.
//

import Foundation
import ObjectMapper

public class WorkTypes:Mappable, Equatable {
    
    
    public /// Returns a Boolean value indicating whether two values are equal.
    ///
    /// Equality is the inverse of inequality. For any values `a` and `b`,
    /// `a == b` implies that `a != b` is `false`.
    ///
    /// - Parameters:
    ///   - lhs: A value to compare.
    ///   - rhs: Another value to compare.
    static func ==(lhs: WorkTypes, rhs: WorkTypes) -> Bool {
        if (lhs.wt_id==rhs.wt_id){
            return true
        } else {
            return false
        }
    }

    public var wt_id : String?
    public var wt_name : String?
    public var wt_price : String?
    public var wt_people : String?
    public var wt_time : String?
    public var wt_spec_cat : String?
    public var wt_extra : String?
    public var wt_group : String?
    public var wt_sroch : String?
    public var wt_part_need : String?
    public var wt_part_name : String?
    public var wt_part_price : String?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let workTypes_list = WorkTypes.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of WorkTypes Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [WorkTypes]
    {
        var models:[WorkTypes] = []
        for item in array
        {
            models.append(WorkTypes(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let workTypes = WorkTypes(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: WorkTypes Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        wt_id = dictionary["wt_id"] as? String
        wt_name = dictionary["wt_name"] as? String
        wt_price = dictionary["wt_price"] as? String
        wt_people = dictionary["wt_people"] as? String
        wt_time = dictionary["wt_time"] as? String
        wt_spec_cat = dictionary["wt_spec_cat"] as? String
        wt_extra = dictionary["wt_extra"] as? String
        wt_group = dictionary["wt_group"] as? String
        wt_sroch = dictionary["wt_sroch"] as? String
        wt_part_need = dictionary["wt_part_need"] as? String
        wt_part_name = dictionary["wt_part_name"] as? String
        wt_part_price = dictionary["wt_part_price"] as? String
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.wt_id, forKey: "wt_id")
        dictionary.setValue(self.wt_name, forKey: "wt_name")
        dictionary.setValue(self.wt_price, forKey: "wt_price")
        dictionary.setValue(self.wt_people, forKey: "wt_people")
        dictionary.setValue(self.wt_time, forKey: "wt_time")
        dictionary.setValue(self.wt_spec_cat, forKey: "wt_spec_cat")
        dictionary.setValue(self.wt_extra, forKey: "wt_extra")
        dictionary.setValue(self.wt_group, forKey: "wt_group")
        dictionary.setValue(self.wt_sroch, forKey: "wt_sroch")
        dictionary.setValue(self.wt_part_need, forKey: "wt_part_need")
        dictionary.setValue(self.wt_part_name, forKey: "wt_part_name")
        dictionary.setValue(self.wt_part_price, forKey: "wt_part_price")
        
        return dictionary
    }
    
    public required init?(map: Map) {
        wt_group <- map["wt_group"]
        wt_spec_cat <- map["wt_spec_cat"]
        wt_part_price <- map["wt_part_price"]
        wt_name <- map["wt_name"]
        wt_id <- map["wt_id"]
        wt_sroch <- map["wt_sroch"]
        wt_time <- map["wt_time"]
        wt_people <- map["wt_people"]
        wt_price <- map["wt_price"]
        wt_part_name <- map["wt_part_name"]
        wt_part_need <- map["wt_part_need"]
        wt_extra <- map["wt_extra"]
        
    }
    
    public func mapping(map: Map) {
        wt_group <- map["wt_group"]
        wt_spec_cat <- map["wt_spec_cat"]
        wt_part_price <- map["wt_part_price"]
        wt_name <- map["wt_name"]
        wt_id <- map["wt_id"]
        wt_sroch <- map["wt_sroch"]
        wt_time <- map["wt_time"]
        wt_people <- map["wt_people"]
        wt_price <- map["wt_price"]
        wt_part_name <- map["wt_part_name"]
        wt_part_need <- map["wt_part_need"]
        wt_extra <- map["wt_extra"]
    }
    
}
