//
//  MyTableViewController.swift
//  GateProject
//
//  Created by Alana on 16.09.17.
//  Copyright © 2017 Alana. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import Toast_Swift

class MyTableViewController: UITableViewController {
    var arrName = [String]()
    var arrWork = [Works]()
    
    var orderID = -1;
    
    let appdelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        appdelegate.addIndicator()
        
        (UIApplication.shared.delegate as! AppDelegate).getBaseDataAsync { (base:Base!) in
            self.arrName.removeAll()
            for work in base.works!{
                self.arrName.append(work.workName!)
                self.arrWork.append(work)
            }
            if (self.arrName.count>0){
                self.tableView.reloadData()
                self.appdelegate.hideIndicator()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier=="toClientDataVC"{
            if let indexPath = self.tableView.indexPathForSelectedRow{
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.workIndex = Int(arrWork[indexPath.row].workID!)!
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrName.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        let n = arrName[indexPath.row]
        cell?.textLabel?.text = n
        return cell!
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

