//
//  ModelsVC.swift
//  GateProject
//
//  Created by Alana on 23.10.17.
//  Copyright © 2017 Alana. All rights reserved.
//

import UIKit

class ModelsVC: UIViewController{
    
    @IBOutlet weak var tableView: UITableView!
    var arrModels = [Models]()
    var modelName = [String]()
    public var modelGroupName: String = ""
    
    public var clientName:String = ""
    public var email: String = ""
    public var organizationName: String = ""
    public var cycles : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        
        if (appdelegate.modelGroupIndex >= 0){
            (UIApplication.shared.delegate as! AppDelegate).getBaseDataAsync(cmpl: { (b:Base) in
                for wg in (b.modelGroups?[appdelegate.modelGroupIndex].models)!{
                    self.arrModels.append(wg)
                    self.modelName.append(wg.modelName!)
                }
                self.tableView.reloadData()
            })
        }
    }
}

extension ModelsVC: UITableViewDataSource, UITableViewDelegate{
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        let models = arrModels[indexPath.row]
        cell?.textLabel?.text = models.modelName
        return cell!
    }
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrModels.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc2 = storyboard?.instantiateViewController(withIdentifier: "WorkListVC") as! WorkListController
        var id = Int( arrModels[indexPath.row].modelID!)!
        (UIApplication.shared.delegate as! AppDelegate).modelId = id
        self.navigationController?.pushViewController(vc2, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
