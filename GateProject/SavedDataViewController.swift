//
//  TestDataViewController.swift
//  GateProject
//
//  Created by user on 26.04.2018.
//  Copyright © 2018 Alana. All rights reserved.
//

import UIKit
import Alamofire

class SavedDataViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var list = [String]()
    var count = 0;
    var appdelegate = UIApplication.shared.delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()
        
        list = appdelegate.list
        count = list.count
    }
   
    @IBAction func sendToServer(_ sender: Any) {
        appdelegate.addIndicator()
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        for item in list{
            do {
                let fullName = documentsPath.appendingFormat("/%@", item)
                let text = try String(contentsOfFile: fullName)
                let dictContents = self.convertToDictionary(text: text)
                
                Alamofire.request("http://msofter.ru/gates/receive.php", method: .post, parameters: dictContents, encoding: URLEncoding.default)
                    .responseString(completionHandler: { (response : DataResponse<String>) in
                        switch response.result{
                            
                        case .failure(let error):
                            self.appdelegate.hideIndicator()
                            print(error)
                            self.view.makeToast("Не отправлено", duration: 3.0, position: CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.width / 5))
                            break
                        case .success(let responseObject):
                            self.appdelegate.hideIndicator()
                            print(responseObject)
                            self.view.makeToast("Отправлено", duration: 3.0, position: CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.width / 5))
                            
                            let fileManager = FileManager.default
                            do {
                                try fileManager.removeItem(atPath: fullName)
                                 self.count -= 1
                                 self.tableView.reloadData()

                                if(self.count == 0){
                                    self.nextScreen()
                                }
                            }
                            catch let error as NSError {
                                print("Something went wrong: \(error)")
                            }
                        }
                    })
            } catch {
                // contents could not be loaded
            }
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    func getFileCount () -> Int {
        if (count>0){
            appdelegate.list = list
            return count
        }
        else{
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let fm = FileManager.default
            do{
                list = try fm.contentsOfDirectory(atPath: documentsPath)
                appdelegate.list = list
                print (list)
                count = list.count
                return list.count
            }
            catch{
            }
            return 0
        }
    }
    
    func nextScreen() {
        self.navigationController?.popToRootViewController(animated: false)
    }
}

extension SavedDataViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {        
        return getFileCount()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.textLabel?.text = list[indexPath.row]
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

