/*
 Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import Foundation
import ObjectMapper

/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class ModelGroups:Mappable {
    public var eq_id : String?
    public var eq_name : String?
    public var eq_descr : String?
    public var eq_extra : String?
    public var models : Array<Models>?
    public var wg : Array<Wg>?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let modelGroups_list = ModelGroups.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of ModelGroups Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [ModelGroups]
    {
        var models:[ModelGroups] = []
        for item in array
        {
            models.append(ModelGroups(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let modelGroups = ModelGroups(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: ModelGroups Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        eq_id = dictionary["eq_id"] as? String
        eq_name = dictionary["eq_name"] as? String
        eq_descr = dictionary["eq_descr"] as? String
        eq_extra = dictionary["eq_extra"] as? String
        if (dictionary["models"] != nil) { models = Models.modelsFromDictionaryArray(array: dictionary["models"] as! NSArray) }
        if (dictionary["wg"] != nil) { wg = Wg.modelsFromDictionaryArray(array: dictionary["wg"] as! NSArray) }
       
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.eq_id, forKey: "eq_id")
        dictionary.setValue(self.eq_name, forKey: "eq_name")
        dictionary.setValue(self.eq_descr, forKey: "eq_descr")
        dictionary.setValue(self.eq_extra, forKey: "eq_extra")
        
        return dictionary
    }
    
    public func mapping(map: Map) {
        eq_id <- map["eq_id"]
        eq_name <- map["eq_name"]
        eq_descr <- map["eq_descr"]
        eq_extra <- map["eq_extra"]
        models <- map["models"]
        wg <- map["wg"]
    }
    
    public required init?(map: Map) {
        eq_id <- map["eq_id"]
        eq_name <- map["eq_name"]
        eq_descr <- map["eq_descr"]
        eq_extra <- map["eq_extra"]
        models <- map["models"]
        wg <- map["wg"]
    }
    
}
