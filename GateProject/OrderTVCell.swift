//
//  OrderTVCell.swift
//  GateProject
//
//  Created by David Minasyan on 10.01.2018.
//  Copyright © 2018 Alana. All rights reserved.
//

import UIKit

class OrderTVCell: UITableViewCell {

    @IBOutlet weak var clientLabel: UILabel!
    @IBOutlet weak var orderNumber: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var adressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
