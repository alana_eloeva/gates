//
//  AppDelegate.swift
//  GateProject
//
//  Created by Alana on 16.09.17.
//  Copyright © 2017 Alana. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import Toast_Swift


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var indicator = UIActivityIndicatorView()
    
    var b : Base?
    let URL = "http://msofter.ru/gates/index.php"
    public var workDiagn = [WorkTypes]()
    public var workRepair = [WorkTypes]()
    public var groupRepair = [Wg]()
    public var groupDiagn = [Wg]()
    public var UserID :String?
    
    public var workIndex: Int = -1
    public var modelGroupIndex = -1
    public var gateType:Int = -1
    public var userId:Int = -1
    public var modelId = 0
    
    public var location: String = ""
    public var orderAdress: String = ""
    public var orderDate: String = ""
    public var orderNumber: String = ""
    public var clientName: String = ""
    public var email: String = ""
    public var order_act_id: String = ""
    public var organizationName: String = ""
    
    public var list = [String]()
    
    func loginDetails(){
        let login: String? = UserDefaults.standard.string(forKey: "login")
        let password: String? = UserDefaults.standard.string(forKey: "pass")

        if(login != nil && password != nil){
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "chooseOrderScreen")
            self.window?.rootViewController = vc
        }else{
            let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let regVC = storyboard.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
            self.window?.rootViewController = regVC
        }
    }
    
    func addIndicator(){
        indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        indicator.frame = CGRect(x: UIScreen.main.bounds.width/2-40, y: UIScreen.main.bounds.height/5, width: 80, height: 80)
        indicator.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        indicator.alpha = 0.75
        indicator.layer.cornerRadius = 10
        showIndicator()
    }
    
    func showIndicator(){
        indicator.startAnimating()
        window?.rootViewController?.view.addSubview(indicator)
    }
    
    func hideIndicator(){
        indicator.stopAnimating()
        indicator.isHidden = true
        indicator.removeFromSuperview()
    }
    
    public func isInWorkDiag(workType: WorkTypes) -> Bool {
        for w in workDiagn {
            if w.wt_id == workType.wt_id && w.wt_name == workType.wt_name && w.wt_group == w.wt_group {
                return true
            }
        }
        return false
    }
    
    public func isInWorkRepair(workType: WorkTypes) -> Bool {
        for w in workRepair {
            if w.wt_id == workType.wt_id && w.wt_name == workType.wt_name && w.wt_group == w.wt_group {
                return true
            }
        }
        return false
    }
    
    public func isInGroupRepair(wg: Wg) -> Bool{
        for w in groupRepair{
            if w.wtg_id == wg.wtg_id && w.wtg_name == wg.wtg_name  {
                return true
            }
        }
        return false
    }
    
    public func isInGroupDiagn(wg: Wg) -> Bool{
        for w in groupDiagn{
            if w.wtg_id == wg.wtg_id && w.wtg_name == wg.wtg_name  {
                return true
            }
        }
        return false
    }
    
    public func addToWorkDiagn(wt: WorkTypes){
        if (!isInWorkDiag(workType: wt)){
            workDiagn.append(wt)
        }
    }
    
    public func removeFromWorkDiagn(wt: WorkTypes){
        if (isInWorkDiag(workType: wt)){
            var m = 0;
            for w in workDiagn{
                if (w.wt_id==wt.wt_id){
                    workDiagn.remove(at: m)
                    return
                }
                m+=1
            }
        }
    }
    
    public func addToWorkRepair(wt: WorkTypes){
        if (!isInWorkRepair(workType: wt)){
            workRepair.append(wt)
        }
    }
    
    public func removeFromWorkRepair(wt: WorkTypes){
        if (isInWorkRepair(workType: wt)){
            var m = 0;
            for w in workRepair{
                if (w.wt_id==wt.wt_id){
                    workRepair.remove(at: m)
                    return
                }
                m+=1
            }
        }
    }
    
    public func addToGroupRepair(wt: Wg){
        if (!isInGroupRepair(wg: wt)){
            groupRepair.append(wt)
            print("Added to group repair")
        }
    }
    
    public func removeFromGroupRepair(wt: Wg){
        if (isInGroupRepair(wg: wt)){
            var m = 0;
            for w in groupRepair{
                if (w.wtg_id==wt.wtg_id){
                    groupRepair.remove(at: m)
                    print("Removed from group repair")
                    return
                }
                m+=1
            }
        }
    }
    
    public func addToGroupDiagn(wt: Wg){
        if (!isInGroupDiagn(wg: wt)){
            groupDiagn.append(wt)
            print("Added to group diagn")
        }
    }
    
    public func removeFromGroupDiagn(wt: Wg){
        if (isInGroupDiagn(wg: wt)){
            var m = 0;
            for w in groupDiagn{
                if (w.wtg_id==wt.wtg_id){
                    groupDiagn.remove(at: m)
                    print("Removed from group diagn")
                    return
                }
                m+=1
            }
        }
    }
    
    func isEmptyField(textField: UITextField, message: String) -> Bool{
        if (textField.text?.isEmpty)! {
            self.window?.makeToast(message, duration: 3.0, position: CGPoint(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.width / 5))
            
            return false
        }else{
            return true
        }
    }
    
    func validateEmail(enteredEmail:String, message: String )->Bool{
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        if  emailPredicate.evaluate(with: enteredEmail) {
            return true
        } else {
            self.window?.makeToast(message, duration: 3.0, position: CGPoint(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.width / 5))
            return false
        }
    }
    
    func validateSerNumber(enteredNumber: String, message: String, type: Int)-> Bool{
        if(type == 2){
        let format = "[a-zA-Z]{2,3}+(\\s)+[0-9]{6}"
        let predicate = NSPredicate(format:"SELF MATCHES %@", format)
        if  predicate.evaluate(with: enteredNumber) {
            return true
        } else {
            self.window?.makeToast(message, duration: 3.0, position: CGPoint(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.width / 5))
            return false
            }
        }else{
            return true
        }
    }
    
    func fetchAPI(completion: @escaping (_ base : Base) -> Void){
        if isConnectedToInternet(){
            Alamofire.request(URL).responseObject { (response: DataResponse<Base>) in
                if let k = response.result.value{
                    self.b = k
                    completion(self.b!)
                }
            }
        }else{
            self.window?.makeToast("Проблемы с сетью. Проверьте подключение и повторите попытку", duration: 3.0, position: CGPoint(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.width / 5))
        }
    }
    
    func getBaseDataAsync(cmpl: @escaping (_ base : Base) -> Void){
        DispatchQueue.main.async {
            self.fetchAPI(completion: cmpl)
        }
    }
    
    func isConnectedToInternet() -> Bool{
        return NetworkReachabilityManager()!.isReachable
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
//        loginDetails()
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    }
    
    // MARK: - Core Data stack
    
    

}

